git config --global user.name "Farzana Tasnim"
git config --global user.email "farzanatasnim34@gmail.com"

Create a new repository

git clone https://gitlab.com/Farzana.tani/farzana_180994_B70_s5_htmlcss.git
cd farzana_180994_B70_s5_htmlcss
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder

cd existing_folder
git init
git remote add origin https://gitlab.com/Farzana.tani/farzana_180994_B70_s5_htmlcss.git
git add .
git commit -m "Initial commit"
git push -u origin master

Existing Git repository

cd existing_repo
git remote add origin https://gitlab.com/Farzana.tani/farzana_180994_B70_s5_htmlcss.git
git push -u origin --all
git push -u origin --tags